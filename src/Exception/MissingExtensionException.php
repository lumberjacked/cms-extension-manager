<?php

namespace Cms\ExtensionManager\Exception;

class MissingExtensionException extends \RuntimeException
{
}