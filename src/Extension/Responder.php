<?php
namespace Cms\ExtensionManager\Extension;

use Zend\Json\Json;
use Zend\Http\Response;
use Zend\View\Model\JsonModel;
use Zend\Stdlib\Hydrator\ObjectProperty;

class Responder {

	public $is_error;

	public $message;

	public $data;

	public $status_code;

	public function __construct($is_error = false, $message = null, $data = array(), $status_code = 200) {
		
		$this->is_error    = $is_error;
		$this->message     = $message;
		$this->data        = $data;
		$this->status_code = $status_code;	
	}

	public function isError() {
		return $this->is_error;
	}

	public function getMessage() {
		return $this->message;
	}

	public function getData() {
		return $this->data;
	}

	public function getStatusCode() {
		return $this->status_code;
	}
	
}