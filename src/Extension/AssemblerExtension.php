<?php
namespace Cms\ExtensionManager\Extension;

use Cms\ExtensionManager\Extension\AbstractAssembler;

class AssemblerExtension extends AbstractAssembler {

    public function __call($name, $arguments) {
        
        if(method_exists($this->custom_handler, $name)) {
                return call_user_func($this->custom_handler->$name(), $arguments);   
            } else {
                throw new \Exception(sprintf('Method {%s} does not exist on %s.', $name, get_class($this->custom_handler)));
            } 
    }
}