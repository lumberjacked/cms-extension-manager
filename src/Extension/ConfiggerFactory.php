<?php
namespace Cms\ExtensionManager\Extension;

use Zend\Config\Config;
use Zend\Validator\File\Exists;
use Zend\EventManager\EventManager;
use Zend\Config\Writer\PhpArray as ConfigWriter;
use ZF\Configuration\ConfigResource;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ConfiggerFactory implements FactoryInterface {
    
    public function createService(ServiceLocatorInterface $serviceLocator) {
        
        $options         = $serviceLocator->get('Cms\ExtensionManager\Options\ModuleOptions');
        $configgerWriter = $serviceLocator->get('Cms\ExtensionManager\Extension\ConfiggerWriter');
        
        $configger = new Configger($options, $serviceLocator->get('Config'));
        
        $local = new Exists($options->getAutoload());
        
        if($local->isValid($configgerWriter::LOCAL_CONFIG)) {
            
            $localConfig = include $options->getAutoload() . '/' . $configgerWriter::LOCAL_CONFIG;
            $localConfig = new Config($localConfig, true);

            $configger->setConfig($localConfig);
        
            $configger->setIsInstalled(true);
        }
        
        return $configger;
    }
}

