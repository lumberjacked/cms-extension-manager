<?php
namespace Cms\ExtensionManager\Extension;

use Zend\Json\Json;
use Zend\Http\Client;
use Zend\Http\Response;
use Zend\Stdlib\Hydrator\ClassMethods;

class DbRequester {

	protected $method;

	protected $resource;

	protected $params;

	protected $hydrator;

	protected $responseMethod;

	public function __construct($method, $resource, array $params = array()) {
		
		$this->method   = $method;
		$this->resource = $resource;
		$this->params   = $params;
		$this->hydrator = new ClassMethods();
		$this->initResponseMethod();

	}
	
	public function getMethod() {
		return $this->method;
	}

	public function getResource() {
		return $this->resource;
	}

	protected function setParams($params) {
		$this->params = $params;
	}

	public function getParams() {
		return $this->params;
	}

	protected function initResponseMethod() {
		$this->responseMethod = $this->resource . '#' . $this->method;
	}

	public function getResponseMethod() {
		return $this->responseMethod;
	}
}

	

	