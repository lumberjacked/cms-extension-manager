<?php
namespace Cms\ExtensionManager\Extension;

use Cms\ExtensionManager\Extension\ExtensionInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\Event;
//use Zend\EventManager\SharedEventManagerInterface;

trait ExtensionTrait {

    protected $name;

    protected $xmanager;
    
    protected $identifier;
    
    protected $options = array();

    protected $listeners = array();

    protected $hasListeners = false;

    public function __construct() {
        $this->identifer = get_called_class();
    }

    public function initialize(Xmanager $xmanager) {
        $this->xmanager = $xmanager;    
    }

    protected function getXmanager() {
        return $this->xmanager;
    }

    public function attach(EventManagerInterface $e) {
        
        //$this->listeners[] = $e->attach('manager.extension.initialize', array($this, 'initialize'), 100);

        $listeners = $this->getListeners();
        if(is_array($listeners) && !empty($listeners)) {
            
            foreach($listeners as $event => $callback) {
                
                $this->listeners[] = $e->attach($event, array($this, $callback), 100);   
            }

            $this->hasListeners = true;
        }

        return $this;
                 
    }

    // public function attachShared(SharedEventManagerInterface $sharedEvents) {
        
    //     $listeners = $this->getSharedListeners();
        
    //     if(is_array($listeners) && !empty($listeners)) {
                
    //         foreach($listeners as $identifer => $callback) {
                
    //             if(is_array($callback)) {
    //                 $event = key($callback);
    //                 $callback = $callback[$event];

    //                 $this->sharedListeners[] = $sharedEvents->attach($identifer, $event, array($this, $callback), 100);   
                    
    //             }   
    //         }
            
    //         $this->hasSharedListeners = true;
    //     }
    // }

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;  
    }

    public function setOptions(array $options) {
        
        $this->options = $options;
        return $this;
    }

    public function getOption($name) {
        $option = null;
        
        if (isset($this->options[$name])) {
            $option = $this->options[$name];
        }
        
        return $option;
    }

    public function getOptions() {
        return $this->options;
    }

    public function hasListeners() {
        return $this->hasListeners;
    }

    public function hasSharedListeners() {
        return $this->hasSharedListeners;
    }

    public function getIdentifier() {
        return get_called_class();
    }

    public function getListeners() {
        return $this->getOption('listeners');
    }

    public function getSharedListeners() {
        return $this->getOption('shared-listeners');
    }
    
}