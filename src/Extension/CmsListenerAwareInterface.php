<?php
namespace Cms\ExtensionManager\Extension;

use Zend\EventManager\EventManagerInterface;

interface CmsListenerAwareInterface {
	
	public function attach(EventManagerInterface $e);
}