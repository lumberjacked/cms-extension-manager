<?php
namespace Cms\ExtensionManager\Extension;

use Cms\ExtensionManager\Extension\AbstractHandler;

interface HandlerExtensionInterface {
    
    public function setHandlerExtension(AbstractHandler $handler);
}