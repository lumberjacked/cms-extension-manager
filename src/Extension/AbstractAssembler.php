<?php 
namespace Cms\ExtensionManager\Extension;

use ZF\ApiProblem\ApiProblem;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Stdlib\Hydrator\ObjectProperty;
use Zend\Stdlib\Hydrator\ArraySerializable;
use Doctrine\Common\Persistence\ObjectManager;
use Cms\ExtensionManager\Extension\AbstractExtension;
/**
 *
 * @author Spencer Bragg <lumberjacked.again@gmail.com>
 * 
 */
abstract class AbstractAssembler extends AbstractExtension {

    /**
     * @var Zend\Stdlib\Hydrator\ClassMethods
     */
    //protected $hydrator;

    /**
     * @var Doctrine Mongo Object
     */
    protected $document;

    /**
     * @var DoctrineObjectManager
     */
    // protected $objectManager;

    protected $apiProblem;

    protected $dataResults;

    public function data($data) {
        
        if($data instanceof ApiProblem) {
            $this->apiProblem = $data;
        } else {
            $this->setDataResults($data);   
        }
        
        return $this;        
    }

    /**
     * Get doctrine repository class 
     *
     * @return string
     */
    // protected function getRepositoryClass() {
    //     return get_class($this->document);
    // }

    /**
     * Set doctrine object manager
     *
     * @param Doctrine\ODM\MongoDB\DocumentManager $objectManager
     * @return void
     */
    // public function setObjectManager(ObjectManager $objectManager) {
    //     $this->objectManager = $objectManager;
    // }

    /**
     * Get doctrine object manager
     *
     * @return Doctrine\ODM\MongoDB\DocumentManager $objectManager
     */
    // protected function getObjectManager() {
    //     return $this->objectManager;
    // }

    /**
     * Set data results
     *
     * @param mixed $results
     * @return Cms\ExtensionManager\Extension\AbstractHandler
     */
    protected function setDataResults($results) {
        
        $this->dataResults = $results;
        return $this;
    }

    /**
     * Get data results
     *
     * @return mixed|DoctrineRepository Entities
     */
    protected function getDataResults() {
        
        if(null !== $this->apiProblem) {
            return $this->apiProblem;
        }

        return $this->dataResults;
    }

    /**
     * Set Doctrine Object
     *
     * @return void
     */
    public function setDocument($document) {
        $this->document = $document;
    }

     /**
      * Get current Doctrine Object
      *
      * @return DoctrineObject
      */
    public function getDocument() {
        return $this->document;
    }

    // public function setApiProblem($problem) {
    //     $this->apiProblem = $problem;
    //     return $this;
    // }

    /**
     * Get assembled data results
     *
     * @return array
     */
    public function extract($hydrator = null) {
        
        $result_queue = $this->getDataResults();
        if($result_queue instanceof ApiProblem) {
            return $result_queue;
        }
        
        $hydrator = new ClassMethods;
        
        $results = array();
        if(!is_array($result_queue)) {
            $results[] = $hydrator->extract($result_queue);

        } else {

            foreach($result_queue as $i => $entity) {
                $results[] = $hydrator->extract($entity);
            }    
        }
        
        return $results;
    }

    //TODO:: abstract all assembler methods into an assembler extension and allow override
    /**
     * Get assembled data results
     *
     * @return array
     */
    // public function hydrate($data) {
        
    //     $hydrator = new ClassMethods;
    //     return $hydrator->hydrate($this->document);
        
    // }   
}