<?php
namespace Cms\ExtensionManager\Extension;

use Zend\Json\Json;
use Zend\Http\Client;
use Zend\Http\Response;

class Requester {

	protected $fqdn;

	protected $method;
	
	protected $resource;

	protected $fqdn_resource;

	protected $params;

	protected $client;

	protected $public;

	protected $id = null;

	protected $headers = array();

	protected $authenticationService = null;
	

	public function __construct($backend, $method, $resource, array $params = array(), $public = false) {

		$this->fqdn     = $backend;
		$this->resource = $resource;
		$this->method   = $method;
		$this->public   = $public;

		if(array_key_exists('id', $params)) {
			$this->id = $params['id'];
			unset($params['id']);
		}

		$this->params   = $params;
	}

    public function setAuthenticationService($authentication) {
        $this->authenticationService = $authentication;
    }

    private function getAuthenticationService() {
        return $this->authenticationService;
    }

	public function initRequester() {
            
        if($this->fqdn == null) {
       		throw new \Exception('FQDN must be defined in local or global configuration');
        }

		if($this->id != null) {
			$fqdn_url = $this->fqdn . '/' . $this->resource . '/' . $this->id;
		} else {
			$fqdn_url = $this->fqdn . '/' . $this->resource;
		}

        $this->setFqdnResource($fqdn_url);
        
        $client = $this->buildApiClient();

        $this->setClient($client);

        return $this;
    }

    protected function buildApiClient() {
        
        $client = new Client($this->getFqdnResource());
        $client->setAdapter('Zend\Http\Client\Adapter\Curl');

        if(in_array($this->getMethod(), array('post', 'put', 'patch', 'delete'))) {
            $client->setMethod($this->getMethod());
            
            $client->setRawBody(Json::encode($this->getParams())); 
            $client->setEncType('application/json');

        }

        $client_headers = $client->getRequest()->getHeaders();
        $client_headers->addHeaderLine('Accept', 'application/json');
        $client_headers->addHeaderLine('Content-Type', 'application/json; charset=utf-8');

        if(!$this->public) {
            
            $identity = $this->getAuthenticationService()->getIdentity();
            
            if($identity == null) {
                return new ApiProblemResponse(ApiProblem(403, 'Forbidden: Token is expired'));
            }

            $token    = $identity->getAccessToken();
            
            $client_headers->addHeaderLine('Authorization', 'Bearer' . ' ' . $token);
            
        }
        
        return $client;
    }

	// public function newRequester($resource, $method, array $params = array()) {
	// 	$this->resource = $resource;
	// 	$this->method   = $method;
		
	// 	$this->headers = array();
	// 	$this->parseParams($params);
	// 	$this->initRequester();

	// 	return $this;	
	// }

	public function request() {
		$client = $this->getClient();
		
		return $this->hydrate($client->send());
	}

	protected function hydrate(Response $response) {
		
		$content = Json::decode($response->getContent(), Json::TYPE_ARRAY);
		
		if($response->isSuccess()) {
			
			// if(array_key_exists('access_token', $content) || array_key_exists('token', $content)) {
			// 	$this->handleAuthenticatedResponse($content);
			// }

			
			return new Responder(!$response->isSuccess(), $response->getReasonPhrase(), $content, $response->getStatusCode());
		}
		
		return new Responder(!$response->isSuccess(), $response->getReasonPhrase(), $content, $response->getStatusCode());

	}

	protected function getClient() {
		return $this->client;
	}

	protected function setFqdnResource($fqdn) {
		$this->fqdn_resource = $fqdn;
	}

	public function getFqdnResource() {
		return $this->fqdn_resource;
	}

	protected function getMethod() {
		return $this->method;
	}

	protected function setClient($client) {
		$this->client = $client;
	}

	// protected function parseParams(array $params) {

	// 	if(array_key_exists('headers', $params)) {
	// 		$headers = $params['headers'];
	// 		$this->setHeaders($headers);
	// 		unset($params['headers']);
	// 	}

	// 	$this->setParams($params);

	// }

	// public function checkAvailableParams(array $keys) {
	// 	$params = $this->getParams();
	// 	foreach($keys as $index => $key) {
	// 		if(!array_key_exists($key, $params)) {
	// 			return new \Exception(sprintf('This client api call requires %s -- none given', $key));
	// 		}
	// 	}

	// 	return true;
	// }

	protected function setParams($params) {
		$this->params = $params;
	}

	public function getParams() {
		return $this->params;
	}

	public function getParam($name) {
		if(array_key_exists($name, $this->getParams())) {
			return $this->params[$name];
		}

		return null;
	}

	protected function setHeaders($headers) {
		$this->headers = $headers;
	}

	protected function getHeaders() {
		return $this->headers;
	}


	

	


	
}

	

	