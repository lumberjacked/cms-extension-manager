<?php

namespace Cms\ExtensionManager\Extension;

use Traversable;
use InvalidArgumentException;
use Zend\Stdlib\ArrayUtils;
use Zend\Stdlib\Parameters;
use Cms\ExtensionManager\Exception;
use Zend\EventManager\EventManagerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Cms\ExtensionManager\Extension\HandlerExtensionInterface;
use Cms\ExtensionManager\Extension\AssemblerInterface;

class Xmanager implements  XmanagerInterface {
    
    const EXTENSION_INITIALIZE = 'manager.extension.initialize';

    const EVENT_EXTENSION_LOAD = 'manager.extension.load';

    const EVENT_EXTENSION_GET  = 'manager.extension.get';

    protected $config;

    protected $services;

    protected $listeners = array(
        'get.cms.config' => 'getConfig',
        'get.object'     => 'getObject'
        );

    protected $extensions = array();

    protected $identifiers = array();

    protected $eventManager;

    
    public function __construct(Configger $config, EventManagerInterface $eventManager, ServiceLocatorInterface $serviceLocator) {
        $this->config        = $config;
        $this->attach($eventManager);
        $this->identifiers[] = get_called_class();
        $this->services = $serviceLocator;
        
    }

    public function getConfig(ResponderEvent $e) {
         return $this->setConfig($this->services->get('Cms\ExtensionManager\Extension\Configger'));
    }

    public function setConfig($config) {
        $this->config = $config;
        return $this->config;
    }

    protected function attach(EventManagerInterface $e) {
        
        $this->setEventManager($e);

        $listeners = $this->getListeners();
        if(is_array($listeners) && !empty($listeners)) {
            
            foreach($listeners as $event => $callback) {
                
                $this->listeners[] = $e->attach($event, array($this, $callback), 100);   
            }
        }

        return $this;
                 
    }

    public function getEventManager() {
        return $this->eventManager;    
    }

    protected function setEventManager(EventManagerInterface $eventManager) {
        $this->eventManager = $eventManager;
    }

    protected function getListeners() {
        return $this->listeners;
    }

    public function add($extension) {
        
        $extension->attach($this->getEventManager());
        $this->extensions[$extension->getName()] = $extension;

        return $this;
    }

    public function loadExtensions() {
        $this->getEventManager()->trigger(static::EVENT_EXTENSION_LOAD);
        return $this;
    }

    public function getExtensions() {
        return $this->extensions;
    }

    

    public function getServiceLocator() {
        return $this->services;
    }

    protected function setIdentifier($name) {
        $this->identifiers[] = $name;
    }

    protected function getIdentifiers() {
        return $this->identifiers;
    }

    public function has($name) {
        
        return (array_key_exists($name, $this->getExtensions()) ? true : false);
    }

    public function get($name) {
        
        if($this->has($name)) {
            
            $extension = $this->extensions[$name];
        
            $this->getEventManager()->trigger(static::EVENT_EXTENSION_GET, $extension);

            return $extension;

        } else if($this->getServiceLocator()->has($name)) {
            
            return $this->getServiceLocator()->get($name);
        
        } else {

            throw new Exception\MissingExtensionException(sprintf(
                    'An extension or service by the name/alias "%s" does not exist',
                    $name
                ));

        }  
    }

    public function getObject(ResponderEvent $e) {
        $name = $e->getParam('name');
        
        if($this->has($name)) {
            
            $extension = $this->extensions[$name];
        
            $this->getEventManager()->trigger(static::EVENT_EXTENSION_GET, $extension);

            return $extension;

        } else if($this->getServiceLocator()->has($name)) {
            
            return $this->getServiceLocator()->get($name);
        
        } else {
            
            throw new Exception\MissingExtensionException(sprintf(
                    'An Extension, Service, or Entity by the name/alias "%s" does not exist',
                    $name
                ));

        }  
    }

    public function getExtension($name) {
        if($this->has($name)) {
            
            $extension = $this->extensions[$name];
        
            $this->getEventManager()->trigger(static::EVENT_EXTENSION_GET, $extension);

            return $extension;

        } else {

            throw new Exception\MissingExtensionException(sprintf(
                    'An extension or service by the name/alias "%s" does not exist',
                    $name
                ));

        }     
    }

    public function trigger($event, $data = array(), $callback = array()) {
        
        if ($data instanceof Traversable) {
            $data = ArrayUtils::iteratorToArray($data);
        }

        if($data instanceof Parameters) {
            $data = $data->toArray();
        }

        if (is_object($data)) {
            $data = (array) $data;
        }

        if (!is_array($data)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid data provided to %s; must be an array or Traversable',
                __METHOD__
            ));
        }

        $responder = $this->getEventManager()->trigger($event, $this, $data, $callback);
        return $responder->last();
    }

    public function responder($event, $params) {
        return $this->trigger($event, $params);
    }

    public function api($method, $resource, $params = array(), $public = false) {
        
        if(is_object($params)) {
            $params = (array) $params;
        }

        $requester = new Requester($this->config->getApiBackend(), $method, $resource, $params, $public);
        
        if(!$public) {
            $requester->setAuthenticationService($this->get('Zend\Authentication\AuthenticationService'));   
        }
        
        $requester->initRequester();
        return $requester->request(); 
    }

    public function handler($document, $handler = null) {
        
        $handler_extension = $this->get('handler-extension');
        
        if(null !== $handler) {
            $handler = $this->get($handler);
            if($handler instanceof HandlerExtensionInterface) {
                $handler->setHandlerExtension($handler_extension);    
            }

            $handler_extension->setCustomHandler($handler);   
        }
        
        $handler_extension->setDocument($document);
        $handler_extension->setObjectManager($this->get('doctrine.documentmanager.odm_default'));
        
        return $handler_extension;
    }

    public function assembler($document, $assembler = null) {

        $assembler_extension = $this->get('assembler-extension');
        
        if(null !== $assembler) {
            $assembler = $this->get($assembler);
            if($assembler instanceof AssemblerExtensionInterface) {
                $assembler->setAssemblerExtension($assembler_extension);    
            }

            $assembler_extension->setCustomAssembler($assembler);   
        }

        return $assembler_extension;    
    }


    
}