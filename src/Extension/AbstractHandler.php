<?php 
namespace Cms\ExtensionManager\Extension;

use ZF\ApiProblem\ApiProblem;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Stdlib\Hydrator\ObjectProperty;
use Zend\Stdlib\Hydrator\ArraySerializable;
use Doctrine\Common\Persistence\ObjectManager;
use Cms\ExtensionManager\Extension\AbstractExtension;
/**
 * AbstractHandler is a simple handler to extend from in a handler extension using doctrine
 * It has most of your basic methods for interaction with an api backend
 *
 * @author Spencer Bragg <lumberjacked.again@gmail.com>
 * 
 */
abstract class AbstractHandler extends AbstractExtension {

    /**
     * @var Doctrine Mongo Object
     */
    protected $document;

    /**
     * @var DoctrineObjectManager
     */
    protected $objectManager;

    protected $assembler;

    protected $custom_handler;

    protected $dataResults;

    // protected $apiProblem;

    /**
     * Set Doctrine Object
     *
     * @return void
     */
    public function setDocument($document) {
        $this->document = $document;
    }

     /**
      * Get current Doctrine Object
      *
      * @return DoctrineObject
      */
    public function getDocument() {
        return $this->document;
    }

    /**
     * Set doctrine object manager
     *
     * @param Doctrine\ODM\MongoDB\DocumentManager $objectManager
     * @return void
     */
    public function setObjectManager(ObjectManager $objectManager) {
        $this->objectManager = $objectManager;
    }

    /**
     * Get doctrine object manager
     *
     * @return Doctrine\ODM\MongoDB\DocumentManager $objectManager
     */
    protected function getObjectManager() {
        return $this->objectManager;
    }

    /**
     * Get doctrine repository class 
     *
     * @return string
     */
    protected function getRepositoryClass() {
        return get_class($this->document);
    }

    /**
     * Set data results
     *
     * @param mixed $results
     * @return Cms\ExtensionManager\Extension\AbstractHandler
     */
    protected function setDataResults($results) {
        $this->dataResults = $results;
        return $this;
    }

    /**
     * Get data results
     *
     * @return mixed|DoctrineRepository Entities
     */
    protected function getDataResults() {
        
        return $this->dataResults;
    }

    public function raw() {
        
        return $this->dataResults;    
    }

    public function data($data) {
        $this->dataResults = $data;
        return $this;   
    }

    public function setCustomHandler($handler) {
        $this->custom_handler = $handler;
    }

    // public function setApiProblem($problem) {
    //     $this->apiProblem = $problem;
    //     return $this;
    // }

    // public function isApiProblem() {
    //     return $this->apiProblem;
    // }

    public function assembler($assembler = null) {

        $assembler_extension = $this->get('assembler-extension');
        
        if(null !== $assembler) {
            $assembler = $this->get($assembler);
            if($assembler instanceof AssemblerExtensionInterface) {
                $assembler->setAssemblerExtension($assembler_extension);    
            }

            $assembler_extension->setCustomAssembler($assembler);   
        }

        $assembler_extension->data($this->dataResults);
        
        return $assembler_extension;    
    }

	/**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
	public function fetchAll() {
        return $this->setDataResults(
			$this->getObjectManager()->getRepository($this->getRepositoryClass())->findAll()
		);
	}

	/**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
	public function fetch($id) {
		
        return $this->setDataResults(
            $this->getObjectManager()->getRepository($this->getRepositoryClass())->findOneBy(array('id' => $id))
        );


        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
	}

	/**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
	public function post($data) {

        if(method_exists($this->custom_handler, 'post')) {
            $this->persist($this->custom_handler->post($this->document, $data));

        } 
        
        return $this;
	}

	/**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
	public function put($id, $data) {
		return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
	}

	/**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function putList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

	/**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
	public function patch($id, $data) {
		return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
	}

	/**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    private function persist($document)	 {

        var_dump($document);die();

        try {
            
            $this->objectManager->persist($document);
            $this->objectManager->flush();
                
        } catch(\Exception $e) {
            $this->setDataResults(new ApiProblem(403, $e->getMessage()));
        }

        $this->setDataResults($document);

        return $this;
    }
}