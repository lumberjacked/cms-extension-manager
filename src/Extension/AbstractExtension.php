<?php
namespace Cms\ExtensionManager\Extension;

use Zend\EventManager\EventManagerInterface;
use Cms\ExtensionManager\Extension\ExtensionInterface;

abstract class AbstractExtension implements ExtensionInterface {

    protected $name;

    protected $eventManager;
    
    protected $identifier;
    
    protected $options = array();

    protected $listeners = array();

    protected $hasListeners = false;

    public function __construct() {
        $this->identifer = get_called_class();
    }

    public function attach(EventManagerInterface $e) {
        
        $this->setEventManager($e);

        $listeners = $this->getListeners();
        if(is_array($listeners) && !empty($listeners)) {
            
            foreach($listeners as $event => $callback) {
                
                $this->listeners[] = $e->attach($event, array($this, $callback), 100);   
            }

            $this->hasListeners = true;
        }

        return $this;
                 
    }

    protected function getEventManager() {
        return $this->eventManager;
    }

    protected function setEventManager(EventManagerInterface $eventManager) {
        $this->eventManager = $eventManager;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;  
    }

    public function setOptions(array $options) {
        
        $this->options = $options;
        return $this;
    }

    public function getOption($name) {
        $option = null;
        
        if (isset($this->options[$name])) {
            $option = $this->options[$name];
        }
        
        return $option;
    }

    public function getOptions() {
        return $this->options;
    }

    public function hasListeners() {
        return $this->hasListeners;
    }

    public function getIdentifier() {
        
        return get_called_class();
    }

    public function getListeners() {
        return $this->getOption('listeners');
    }

    protected function trigger($event, $argv = array(), $callback = array()) {
        $responseCollection = $this->getEventManager()->trigger($event, $this, $argv, $callback);
        return $responseCollection->last();
    }

    public function get($name) {
        $responseCollection = $this->getEventManager()->trigger('get.object', $this, array('name' => $name));
        return $responseCollection->last();
    }

    public function handler($document, $handler = null) {
        
        $handler_extension = $this->get('handler-extension');
        
        if(null !== $handler) {
            $handler = $this->get($handler);
            if($handler instanceof HandlerExtensionInterface) {
                $handler->setHandlerExtension($handler_extension);    
            }

            $handler_extension->setCustomHandler($handler);   
        }
        
        $handler_extension->setDocument($document);
        $handler_extension->setObjectManager($this->get('doctrine.documentmanager.odm_default'));
        
        return $handler_extension;
    }

    public function assembler($document, $assembler = null) {

        $assembler_extension = $this->get('assembler-extension');
        
        if(null !== $assembler) {
            $assembler = $this->get($assembler);
            if($assembler instanceof AssemblerExtensionInterface) {
                $assembler->setAssemblerExtension($assembler_extension);    
            }

            $assembler_extension->setCustomAssembler($assembler);   
        }

        return $assembler_extension;    
    }
}