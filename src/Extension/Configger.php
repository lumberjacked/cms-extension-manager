<?php 
namespace Cms\ExtensionManager\Extension;

use Zend\Config\Config;
use Zend\EventManager\EventManager;

use Zend\EventManager\EventManagerInterface;
use Cms\ExtensionManager\Options\ModuleOptions;
use Zend\Config\Writer\PhpArray as ConfigWriter;
use Zend\ServiceManager\ServiceLocatorInterface;

class Configger implements ConfiggerInterface {

    protected $n;

    protected $options;

    protected $config;

    protected $zfconfig;

    protected $isInstalled = false;

    protected $drivers = array('sqlite' => 'pdo_sqlite', 'mongodb' => 'pdo_mongodb');
    
	public function __construct(ModuleOptions $options, $zfconfig) {
        $this->options      = $options;
        $this->zfconfig     = $zfconfig;
	}

    public function getZfconfig() {
        return $this->zfconfig;
    }

    public function getConfig() {
        return $this->config;
    }

    public function setConfig(Config $config) {
        $this->config = $config;

        return $this->config;
    }

	public function getOptions() {
        return $this->options;
    }

    public function setOptions(ModuleOptions $options) {
        $this->options = $options;
    }

    public function isInstalled() {
        return $this->isInstalled;
    }

    public function setIsInstalled($bool) {
        $this->isInstalled = $bool;
    }

    public function getExtensions() {
    	return $this->options->getExtensions();
    }

    public function getApiBackend() {
        return $this->options->getBackend();
    }

    public function getGrantType() {

        return $this->options->getGrantType();
    }

    // public function getAcl() {
    //     return $this->options->getAcl();
    // }

    // public function getRoles() {
    //     $acl = $this->options->getAcl();

    //     if(array_key_exists('roles', $acl)) {
    //         return $acl['roles'];
    //     } else {
    //         return array();
    //     }
    // }

    // public function getResources() {
    //     $acl = $this->options->getAcl();

    //     if(array_key_exists('resources', $acl)) {
    //         return $acl['resources'];
    //     } else {
    //         return array();
    //     }   
    // }

    public function getAutoload() {
    	return $this->options->getAutoload();
    }

    // public function getFqdn() {
    //     $config = $this->getZfConfig();

    //     if(array_key_exists('fqdn', $config['bas_cms']['config'])) {
    //         return $config['bas_cms']['config']['fqdn'];
    //     }
    // }

    // public function getApiBackend() {
    //     $config = $this->getZfConfig();
        
    //     if(array_key_exists('fqdn_api', $config['bas_cms']['config']) && null != $config['bas_cms']['config']['fqdn_api'] || '' !== $config['bas_cms']['config']['fqdn_api']) {
            
    //         return $config['bas_cms']['config']['fqdn_api'];    
    //     } 

    //     return $config['bas_cms']['config']['fqdn']; 
    // }

    public function getDataDirectory() {
        return $this->options->getData();
    }

    public function getDoctrinePaths() {
        $zfConfig = $this->getZfConfig();
        return $zfConfig['doctrine']['driver']['application_entities']['paths'];
    }

    public function getDocumentPaths() {
        $zfConfig = $this->getZfConfig();
        return $zfConfig['doctrine']['driver']['odm_default']['paths'];
    }

    public function getDoctrineDriver() {
        
        $dbtype = $this->config['bas_cms']['config']['database']['dbtype'];
        
        if(array_key_exists($dbtype, $this->drivers)) {
             return $this->drivers[$dbtype];
        } 

        return null;

    }

    public function getDbType() {
        return $this->config['bas_cms']['config']['database']['dbtype'];   
    }

    public function getDoctrineParams() {
        $dbParams = array(
            'driver' => $this->getDoctrineDriver()
        );
        
        $config = $this->getConfig()->toArray();
        
        $dbParams = array_merge($dbParams, $config['doctrine']['connection']['orm_default']['params']);
        
        return $dbParams;
    }
}