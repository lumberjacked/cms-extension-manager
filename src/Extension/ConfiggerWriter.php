<?php 
namespace Cms\ExtensionManager\Extension;

use Zend\Config\Config;
use ZF\Configuration\ConfigResource;
use Zend\Config\Writer\PhpArray as ConfigWriter;


class ConfiggerWriter extends AbstractExtension {

	const LOCAL_CONFIG = 'cms.local.php';

    const OAUTH_CONFIG = 'local.php';

	public function getConfigResource($data, $path) {
        return new ConfigResource($data, $path, new ConfigWriter());
    }


    public function createCmsConfig(ResponderEvent $e) {
        $params   = $e->getParams();
        
        $response = $this->createNewCmsConfiguration($params);
        if($response instanceof \Exception) {
            return $e->responder(null, true, $response->getMessage(), $params, 500);
        }

        $response = $this->createAuthenticationConfiguration();
        if($response instanceof \Exception) {
            return $e->responder(null, true, $response->getMessage(), $params, 500);
        }
        
        
        return $e->responder('cms.local.config', false, 'created cms.local config', array('config' => $this->trigger('get.cms.config')));

    }

    protected function createNewCmsConfiguration(array $params = array()) {
        
        $config   = $this->trigger('get.cms.config');
        $zfconfig = $config->getZfconfig();

        $new_config = new Config(array(), true);
        $new_config->bas_cms = array();
        $new_config->bas_cms->config = array();
        $new_config->bas_cms->config->servers = array();
        $new_config->bas_cms->config->database = array();
        
        if(array_key_exists('fqdn', $params)) {
            $new_config->bas_cms->config->servers->fqdn = $params['fqdn'];
            unset($params['fqdn']);    
        }

        if(array_key_exists('fqdn_api', $params)) {
            $new_config->bas_cms->config->servers->fqdn_api = $params['fqdn_api'];
            unset($params['fqdn_api']);    
        }

        if(array_key_exists('dbtype', $params)) {
            $params = $this->createDbDriver($params, $zfconfig);

            $new_config->doctrine = array();
            $new_config->doctrine->connection = array();
            $new_config->doctrine->connection->orm_default = $params['orm_default'];
            unset($params['orm_default']);

            $new_config->bas_cms->config->database = $params;
        }
        
         
        
        return $this->writeToFile(static::LOCAL_CONFIG, $config->getAutoload(), $new_config->toArray());
    }

    protected function createAuthenticationConfiguration() {

        $config = $this->trigger('get.cms.config');
        $dbtype = $config->getDbType();
        
        $auth_config = include $this->trigger('get.cms.config')->getAutoload() . '/' . static::OAUTH_CONFIG;
        $auth_config = new Config($auth_config, true);

        $auth_config['zf-oauth2']['db']             = array();
        $auth_config['zf-oauth2']['storage']        = 'ZF\\OAuth2\\Adapter\\PdoAdapter';
        $auth_config['zf-oauth2']['db']['dsn_type'] = 'PDO';
        $auth_config['zf-oauth2']['db']['dsn']      = $dbtype . ':' . $config->getDataDirectory() . '/cms.db';
        
        return $this->writeToFile(static::OAUTH_CONFIG, $config->getAutoload(), $auth_config->toArray());

    }

    protected function writeToFile($filename, $location, array $array) {

        try{
            
            $writer = new \Zend\Config\Writer\PhpArray();
            $writer->toFile($location . '/' . $filename, $array);    

        } catch (\Exception $e) {
            return $e;
        }

        return true;
    }

    

    // protected function getNewConfiguration($type, array $params = array()) {
        
        
    // }

    protected function createDbDriver(array $params, array $zfconfig) {
        
        if(array_key_exists($params['dbtype'], $zfconfig['doctrine']['connection'])) {
            $params['orm_default'] = $zfconfig['doctrine']['connection'][$params['dbtype']];

        } else {
        	$params['orm_default'] = null;
        }

        return $params;
    }

    // public function getDoctrinePaths() {
    //     $zfConfig = $this->getZfConfig();
    //     return $zfConfig['doctrine']['driver']['application_entities']['paths'];
    // }

    // public function getDoctrineDriver() {
        
    //     $dbtype = $this->config['bas_cms']['config']['dbtype'];
        
    //     if(array_key_exists($dbtype, $this->drivers)) {
    //          return $this->drivers[$dbtype];
    //     } 

    //     return null;

    // }

    // public function getDoctrineParams() {
    //     $dbParams = array(
    //         'driver' => $this->getDoctrineDriver()
    //     );
        
    //     $config = $this->getConfig()->toArray();
        
    //     $dbParams = array_merge($dbParams, $config['doctrine']['connection']['orm_default']['params']);

    //     return $dbParams;
    // }

}