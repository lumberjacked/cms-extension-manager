<?php
namespace Cms\ExtensionManager\Extension;

use Cms\ExtensionManager\Extension\AbstractHandler;

class HandlerExtension extends AbstractHandler {

    public function __call($method, $arguments) {
        
        if(method_exists($this->custom_handler, $method)) {
            return $this->data(call_user_func_array(array($this->custom_handler, $method), $arguments));   
        } else {
            throw new \Exception(sprintf('Method {%s} does not exist on %s.', $method, get_class($this->custom_handler)));
        }

        return $this; 
    }
}