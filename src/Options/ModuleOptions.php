<?php
namespace Cms\ExtensionManager\Options;

use Zend\Stdlib\AbstractOptions;

class ModuleOptions extends AbstractOptions {
    
    protected $config  = array();
    
    protected $data;

    protected $autoload;

    protected $extensions = array();

    protected $backend;

    protected $acl = array();

    protected $grant_type;

    public function getData() {
        return $this->data;
    }

    public function setData($directory) {
        $this->data = $directory;
    }

    public function getAutoload() {
        return $this->autoload;
    }

    public function setAutoload($directory) {
        $this->autoload = $directory;
    }

    public function getExtensions() {
        return $this->extensions;
    }

    public function setExtensions($extensions) {
        $this->extensions = $extensions;
        return $this;
    }

    public function getConfig() {
        return $this->config;
    }

    public function setConfig($config) {
        $this->config = $config;
    }

    public function getBackend() {
        return $this->backend;
    }

    public function setBackend($backend) {
        $this->backend = $backend;
    }

    public function setGrantType($grant) {
        $this->grant_type = $grant;
    }

    public function getGrantType() {
        return $this->grant_type;
    }
}