<?php
namespace Cms\ExtensionManager\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Cms\ExtensionManager\Extension\ResponderMapper;

class ResponderMapperFactory {
    
    public function __invoke(ServiceLocatorInterface $services) {
        
    	return new ResponderMapper($services->get('Cms\ExtensionManager\Extension\XmanagerExtension'));   
    }
}