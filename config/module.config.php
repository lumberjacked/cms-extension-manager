<?php

return array(
    'controller_plugins' => array(
        'factories' => array(
            'cms.extension.plugin' => 'Cms\ExtensionManager\Controller\Plugin\ExtensionPluginFactory'
        )
    ),

    'extensions_config' => array(
        'data' => getcwd() . '/data',
        'autoload' => getcwd() . '/config/autoload',
        'extensions' => array(
            'config-writer' => array(
                'type'    => 'Cms\ExtensionManager\Extension\ConfiggerWriter',
                'options' => array(
                    'listeners' => array(
                        'cms.config.create'  => 'createCmsConfig'
                    )
                ),
            ),
            'handler-extension' => array(
                'type' => 'Cms\ExtensionManager\Extension\HandlerExtension',
                'options' => array(

                )
            ),
            'assembler-extension' => array(
                'type' => 'Cms\ExtensionManager\Extension\AssemblerExtension',
                'options' => array(

                )
            )
        )
    ),
);